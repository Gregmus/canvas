import {AbstractElement} from "./AbstractElement";

export class Actions{

    static spinning(object: AbstractElement, object_b, vSpeed: number = 0.01, hSpeed: number = vSpeed){
        return function () {
            object.moveTo(
                object_b.x + (object.x - object_b.x) * Math.cos(vSpeed) - (object.y - object_b.y) * Math.sin(hSpeed),
                object_b.y + (object.x - object_b.x) * Math.sin(hSpeed) + (object.y - object_b.y) * Math.cos(vSpeed)
            );
        };
    }

    static stay(object: AbstractElement, object_b){
        return function () {
            if (object.x != object_b.x && object.y != object_b.y) {
                object.moveTo(object_b.x, object_b.y);
            }
        };
    }
}