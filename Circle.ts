import {AbstractElement} from "./AbstractElement";

export class Circle extends AbstractElement{
    r: number;
    actions: Array<Function> = [];

    constructor(x: number, y: number, r: number){
        super(x, y, r*2, r*2);
        this.r = r;
    }

    addAction(action: Function){
        this.actions.push(action);
    }

    draw(){
        this.clearCanvas();
        this.context.beginPath();
        this.context.arc(this.w/2, this.h/2, this.r, 0, 2 * Math.PI);
        this.context.fill();
        this.changed = false;
        return this.canvas;
    }

    moveTo(x: number, y: number){
        this.old_x = this.x;
        this.old_y = this.y;
        this.x = x;
        this.y = y;
        this.changed = true;
    }

    action(){
        for (let i = 0; i < this.actions.length; i++){
            this.actions[i](this);
        }
    }
}