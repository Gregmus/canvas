import {AbstractElement} from "./AbstractElement";
import {Circle} from "./Circle";
import {Actions} from "./Actions";

export class App{
    static canvas: HTMLCanvasElement;
    static ctx: CanvasRenderingContext2D;
    static elements: Array<AbstractElement> = [];
    static x: number;
    static y: number;

    static init(){
        App.canvas = document.createElement("canvas");
        App.canvas.width = window.innerWidth;
        App.canvas.height = window.innerHeight;
        App.canvas.addEventListener('contextmenu', event => event.preventDefault());
        document.body.appendChild(App.canvas);
        App.ctx = App.canvas.getContext('2d');
        App.x = App.canvas.width / 2;
        App.y = App.canvas.height / 2;
        let core = new Circle(App.x, App.y, 5);
        core.addAction(Actions.stay(core, App));
        App.addElement(core);
        App.canvas.onmouseup = function (e) {
            App.moveCenterTo(e.pageX, e.pageY);
        }
    }

    static addElement(el: AbstractElement){
        App.elements.push(el);
    }

    static loop(){
        App.action();
        App.elements.forEach(function (el) {
            if (el.isChanged()) {
                App.ctx.clearRect(el.old_x, el.old_y, el.old_w, el.old_h);
                App.ctx.drawImage(el.draw(), el.x, el.y);
            }
        });
        requestAnimationFrame(App.loop);
    }

    static action(){
        App.elements.forEach(function (el) {
            el.action();
        })
    }

    static moveCenterTo(x: number, y: number){
        App.x = x;
        App.y = y;
    }
}