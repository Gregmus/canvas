export abstract class AbstractElement{
    x: number;
    old_x: number;
    y: number;
    old_y: number;
    w: number;
    old_w: number;
    h: number;
    old_h: number;
    protected canvas: HTMLCanvasElement;
    protected context: CanvasRenderingContext2D;
    protected changed: boolean = true;

    constructor(x: number, y: number, w: number, h: number){
        this.x = x;
        this.old_x = x;
        this.y = y;
        this.old_y = y;
        this.w = w;
        this.old_w = w;
        this.h = h;
        this.old_h = h;
        this.canvas = document.createElement('canvas');
        this.canvas.width = w;
        this.canvas.height = h;
        this.context = this.canvas.getContext('2d');
    }

    getCanvas(){
        return this.canvas;
    }

    isChanged(){
        return this.changed;
    }

    clearCanvas(){
        this.context.clearRect(0, 0, this.w, this.h);
    }

    abstract draw();
    abstract moveTo(x: number, y: number);
    abstract action();
}