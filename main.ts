import {App} from "./App";
import {Circle} from "./Circle";
import {Actions} from "./Actions";

App.init();

let circle = new Circle(App.canvas.width/2-50, App.canvas.height/2-50, 20);
circle.addAction(Actions.spinning(circle, App, 0.05));
let moon = new Circle(circle.x + 25, circle.y + 25, 3);
moon.addAction(Actions.spinning(moon, circle, 0.005));
App.addElement(circle);
App.addElement(moon);

App.loop();

